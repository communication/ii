/*
 * (C)opyright MMV-MMVI Anselm R. Garbe <garbeam at gmail dot com>
 * (C)opyright MMV-MMVI Nico Golde <nico at ngolde dot de>
 * (C)opyright MMVI Christian Dietrich <stettberger at brokenpipe.de>
 * See LICENSE file for license details.
 */

#include <errno.h>
#include <netdb.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <stdio.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <limits.h>
#include <fcntl.h>
#include <string.h>
#include <pwd.h>
#include <signal.h>
#include <sys/stat.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <ctype.h>
#include <time.h>
#include <unistd.h>
#include <stdarg.h>
#include <sys/utsname.h>


#ifndef nil
#define nil NULL		/* for those who don't understand, nil is used in Plan9 */
#endif
#ifndef PIPE_BUF		/* FreeBSD don't know PIPE_BUF */
#define PIPE_BUF 4096
#endif
#define NULL_TEST(a) if(!(a)){perror("ii: Couldn't allocate memory"); return;}
#define NULL_TEST_ARG(a,b) if(!(a)){perror("ii: Couldn't allocate memory"); return (b);}
#define FALSE 0
#define TRUE 1
#define CLIENT_VERSION ""VERSION""
#define CLIENT_SOURCE "http://cvs.brokenpipe.de/cgi-bin/viewcvs.cgi/ii/"
#define USER_INFO "unkown"
#define DCCPROG "/usr/local/bin/ii-dcc.py"


enum
{ TOK_NICKSRV =
    0, TOK_USER, TOK_CMD, TOK_CHAN, TOK_ARG, TOK_TEXT, TOK_USERHOST, TOK_SRV,
  TOK_NICK, TOK_LAST
};

struct _List
{
  void *data;
  struct _List *next;
};
typedef struct _List List;

typedef struct Buddy Buddy;
struct Buddy
{
  char *name;
  char *laststatus;
  char *dcc_ip;
  char *dcc_port;
  char *dcc_size;
  char *dcc_name;
  int dcc_number;
};


typedef struct Channel Channel;
struct Channel
{
  int fd;
  char *name;
  List *users;			/* List of char * */
  Channel *next;
};

static int irc;
#define PING_TIMEOUT 300
static time_t last_response;
static Channel *channels = nil;
static List *dcc_send;
static int dcc_number = 0;

static char *host = "irc.freenode.net";
static char nick[32];		/* might change while running */
static char path[_POSIX_PATH_MAX];
static char message[PIPE_BUF];	/* message buf used for communication */
static char *fullname = NULL;
static char *dcc_port = "5000";
static char *ownip = NULL;
/*{{{ usage */
static void
usage()
{
  fprintf(stderr, "%s",
	  "ii - irc it - " VERSION "\n"
	  "(C)opyright MMVI Anselm R. Garbe, Nico Golde, Christian Dietrich\n"
	  "usage: ii [-i <irc dir>] [-s <host>] [-p <port>]\n"
	  "          [-n <nick>] [-k <password>] [-f <fullname>]\n");
  exit(EXIT_SUCCESS);
}/*}}}*/
/*{{{ Single linked list */

List *
list_append(List * list, void *data)
{
  List *new, *old = list;
  new = calloc(1, sizeof(*new));
  NULL_TEST_ARG(new, NULL);
  new->data = data;
  new->next = NULL;
  if (!old)
    old = new;
  else
    {
      for (; list->next; list = list->next);
      list->next = new;
    }
  return old;
}

List *
list_remove(List * list, void *data)
{
  List *tmp, *tmp2;
  if (!list)
    return NULL;
  if (list->data == data)
    {
      tmp = list->next;
      free(list);
      return tmp;
    }
  for (tmp = list; tmp && tmp->next; tmp = tmp->next)
    {
      if (tmp->next->data == data)
	{
	  tmp2 = tmp->next;
	  tmp->next = tmp2->next;
	  free(tmp2);
	}
    }
  return list;
}

/*}}}*/
/*{{{ Channel Operations*/
Channel *
get_channel(char *name)
{
  Channel *c;
  for (c = channels; c; c = c->next)
    if (!strcmp(name, c->name))
      return c;
  return nil;
}

void *
is_user_on(Channel * c, char *name)
{
  if (!name) return FALSE;
  List *l;
  Buddy *b;
  for (l = c->users; l; l = l->next)
    {
      b = (Buddy *) l->data;
      if (!strcmp(b->name, name))
	return b;
    }
  return FALSE;
}
/*}}}*/
/* {{{ Ip2num conversion */
char *
num2ip(char *num)
{
  uint32_t number;
  int i;
  char *ret = malloc(16), numtmp[5];
  *ret = 0;

  number = (uint32_t) strtoul(num, (char **) NULL, 10);

  for (i = 24; i >= 0; i -= 8)
    {
      snprintf(numtmp, 5, "%d.", (number >> i) & 0xFF);
      ret = strcat(ret, numtmp);
    }
  ret[strlen(ret) - 1] = 0;
  return ret;

}

char *
ip2num(const char *ipnum)
{
  char *ip = strdup(ipnum);
  char *blocks[4], *p, *num = malloc(32);
  int blocklen = 0, i;
  blocks[0] = ip;
  uint32_t number = 0;


  while ((p = strchr(blocks[blocklen], '.')) && blocklen < 4)
    {
      *p = 0;
      blocklen++;
      blocks[blocklen] = ++p;
    }
  if (blocklen < 3)
    return NULL;

  for (i = 0; i < 4; i++)
    {
      number |= (atoi(blocks[i]) << i * 8);
    }
  snprintf(num, 32, "%d", htonl(number));
  free(ip);
  return num;
}

/* }}} */
/*{{{ Upper and lower*/
static char *
lower(char *s)
{
  char *p;
  for (p = s; p && *p; p++)
    *p = tolower(*p);
  return s;
}

static char *
upper(char *s)
{
  char *p;
  for (p = s; p && *p; p++)
    *p = toupper(*p);
  return s;
}				/*}}} */
/*{{{ Filesystem Functions */
/* creates directories top-down, if necessary */
static void
create_dirtree(const char *dir)
{
  char tmp[256];
  char *p;
  size_t len;

  snprintf(tmp, sizeof(tmp), "%s", dir);
  len = strlen(tmp);
  if (tmp[len - 1] == '/')
    tmp[len - 1] = 0;
  for (p = tmp + 1; *p; p++)
    if (*p == '/')
      {
	*p = 0;
	mkdir(tmp, S_IRWXU);
	*p = '/';
      }
  mkdir(tmp, S_IRWXU);
}

static int
get_filepath(char *filepath, size_t len, char *channel, char *file)
{
  if (channel)
    {
      if (!snprintf(filepath, len, "%s/%s", path, lower(channel)))
	return 0;
      create_dirtree(filepath);
      return snprintf(filepath, len, "%s/%s/%s", path, lower(channel), file);
    }
  return snprintf(filepath, len, "%s/%s", path, file);
}

static void
create_filepath(char *filepath, size_t len, char *channel, char *suffix)
{
  if (!get_filepath(filepath, len, channel, suffix))
    {
      fprintf(stderr, "%s", "ii: path to irc directory too long\n");
      exit(EXIT_FAILURE);
    }
}

static int
open_channel(char *name)
{
  static char infile[256];
  create_filepath(infile, sizeof(infile), name, "in");
  unlink(infile);
  if (access(infile, F_OK) == -1)
    mkfifo(infile, S_IRWXU);
  return open(infile, O_RDONLY | O_NONBLOCK, 0);
}

/*}}}*/
/*{{{ Channel Handling*/
static void
add_channel(char *name)
{
  Channel *c;
  int fd;

  for (c = channels; c; c = c->next)
    if (!strcmp(name, c->name))
      return;			/* already handled */

  fd = open_channel(name);
  if (fd == -1)
    {
      perror("ii: cannot create in channel");
      return;
    }
  c = calloc(1, sizeof(Channel));
  if (!c)
    {
      perror("ii: cannot allocate memory");
      exit(EXIT_FAILURE);
    }
  if (!channels)
    channels = c;
  else
    {
      c->next = channels;
      channels = c;
    }
  c->fd = fd;
  c->name = strdup(name);
}

static void
rm_channel(Channel * c)
{
  Channel *p;
  if (channels == c)
    channels = channels->next;
  else
    {
      for (p = channels; p && p->next != c; p = p->next);
      if (p->next == c)
	p->next = c->next;
    }
  free(c->name);
  free(c);
}				/*}}} */
/*{{{ login */
static void
login(char *key, char *fullname)
{
  if (key)
    snprintf(message, PIPE_BUF,
	     "PASS %s\r\nNICK %s\r\nUSER %s localhost %s :%s\r\n", key,
	     nick, nick, host, fullname ? fullname : nick);
  else
    snprintf(message, PIPE_BUF, "NICK %s\r\nUSER %s localhost %s :%s\r\n",
	     nick, nick, host, fullname ? fullname : nick);
  write(irc, message, strlen(message));	/* login */
}/*}}}*/
/*{{{ TCP stuff*/
static int
tcpopen(unsigned short port)
{
  int fd, i;
  struct sockaddr_in6 sin6;
  struct sockaddr_in sin;
  struct hostent *hp;

  memset(&sin, 0, sizeof(struct sockaddr_in));
  memset(&sin6, 0, sizeof(struct sockaddr_in6));

  /* Try IPv6 */
  hp = gethostbyname2(host, AF_INET6);
  if (hp != nil) {
    sin6.sin6_family = AF_INET6;
    sin6.sin6_port = htons(port);
    if ((fd = socket(AF_INET6, SOCK_STREAM, 0)) < 0)
      goto no_socket;
    goto resolved_host6;
  }
  /* Try IPv4 */
  hp = gethostbyname2(host, AF_INET);
  if (hp != nil) {
    sin.sin_family = AF_INET;
    sin.sin_port = htons(port);
    if ((fd = socket(AF_INET, SOCK_STREAM, 0)) < 0)
      goto no_socket;
    goto resolved_host;
  }

  /* Could resolv any hostname, will die now */
  perror("ii: cannot retrieve host information");
  exit(EXIT_FAILURE);
resolved_host6:
  for (i = 0; hp->h_addr_list[i]; i++) {
    memcpy(&sin6.sin6_addr, hp->h_addr_list[i], hp->h_length);
    if (connect(fd, (const struct sockaddr *) &sin6, sizeof(sin6)) >= 0) {
      return fd;
    }
  }
  goto connect_failed;
resolved_host:
  /* Try all returned addresses */
  for (i = 0; hp->h_addr_list[i]; i++) {
    memcpy(&sin.sin_addr, hp->h_addr_list[i], hp->h_length);
    if (connect(fd, (const struct sockaddr *) &sin, sizeof(sin)) >= 0) {
      return fd;
    }
  }
connect_failed:
  /* Couldn't connect to any host, will die now */
  perror("ii: cannot connect to host");
  exit(EXIT_FAILURE);
no_socket:
  perror("ii: couldn't create socket");
  exit(EXIT_FAILURE);
}
/*}}}*/
/*{{{ Tokenize: FIXME*/
static size_t
tokenize(char **result, size_t reslen, char *str, char delim)
{
  char *p, *n;
  size_t i;

  if (!str)
    return 0;
  for (n = str; *n == ' '; n++);
  p = n;
  for (i = 0; *n != 0;)
    {
      if (i == reslen)
	return 0;
      if (*n == delim)
	{
	  *n = 0;
	  result[i++] = p;
	  p = ++n;
	}
      else
	n++;
    }
  if (i < reslen && p < n && strlen(p))
    result[i++] = p;
  return i;			/* number of tokens */
}
/*}}}*/
/*{{{ User Output*/
static void
print_out(char *channel, char *buf)
{
  static char outfile[256];
  FILE *out;
  static char buft[8];
  unsigned int count = 0;
  time_t t = time(0);
  for (; count < strlen(buf); count++)
    if (buf[count] == 2)
      buf[count] = '_';
  create_filepath(outfile, sizeof(outfile), channel, "out");
  out = fopen(outfile, "a");
  strftime(buft, sizeof(buft), "%R", localtime(&t));
  fprintf(out, "%s %s\n", buft, buf);
  fclose(out);
}/*}}}*/
/*{{{ Sending privmsg*/
static void
proc_channels_privmsg(char *channel, char *buf)
{
  Channel *c;
  char *out = "", *p, *px;

  snprintf(message, PIPE_BUF, "<%s> %s", nick, buf);
  for (c = channels; c; c = c->next)
    if (!strcmp(channel, c->name))
      out = channel;
  if (out[0] == 0)
    snprintf(message, PIPE_BUF, ">%s< %s", channel, buf);
  if (buf && !strncmp("\001ACTION", buf, 7))
    {
      p = strdup(buf);
      p += 8;
      px = strchr(p, 1);
      if (px)
	*px = 0;
      snprintf(message, PIPE_BUF, "* %s %s", nick, p ? p : "");
      free(p - 8);
    }
  print_out(out, message);
  snprintf(message, PIPE_BUF, "PRIVMSG %s :%s\r\n", channel, buf);
  write(irc, message, strlen(message));

}/*}}}*/
/*{{{ User Input: Normal input*/
static void
proc_channels_input(Channel * c, char *buf)
{
  static char infile[256];
  char *p;
  List *l;
  Buddy *b;
  if (buf[0] != '/' && buf[0] != 0)
    {
      proc_channels_privmsg(c->name, buf);
      return;
    }
  if (!strncmp(&buf[1], "j ", 2))
    {
      p = strchr(&buf[3], ' ');
      if (p)
	*p = 0;
      if (buf[3] == '#')
	{
	  snprintf(message, PIPE_BUF, "JOIN %s\r\n", &buf[3]);
	  write(irc, message, strlen(message));
	  add_channel(&buf[3]);
	  snprintf(message, PIPE_BUF, "WHO %s\r\n", &buf[3]);
	}
      else
	{
	  if (p)
	    {
	      add_channel(&buf[3]);
	      proc_channels_privmsg(&buf[3], p + 1);
	      return;
	    }
	}
    }
  else if (!strncmp(&buf[1], "users", 5))
    {
      message[0] = 0;
      for (l = c->users; l; l = l->next)
	{
	  b = l->data;
	  if ((strlen(message) + strlen(b->name) + 4) > PIPE_BUF)
	    break;
	  strcat(message, "[");
	  strcat(message, b->name);
	  strcat(message, "] ");
	}
      print_out(c->name, message);
      return;
    }
  else if (!strncmp(&buf[1], "t ", 2))
    {
      snprintf(message, PIPE_BUF, "TOPIC %s :%s\r\n", c->name, &buf[3]);
    }
  else if (!strncmp(&buf[1], "m ", 2) || !strncmp(&buf[1], "msg ", 4))
    {
      char *begin = strchr(&buf[1], ' ');
      begin++;
      p = strchr(begin, ' ');
      if (p)
	{
	  *p = 0;
	  proc_channels_privmsg(begin, p + 1);
	}
      return;
    }
  else if (!strncmp(&buf[1], "me ", 3))
    {
      snprintf(message, PIPE_BUF, "\001ACTION %s\001", &buf[4]);
      p = strdup(message);
      proc_channels_privmsg(c->name, p);
      free(p);
      return;
    }
  else if (!strncmp(&buf[1], "ctcp", 4))
    {
      char *recv, *command, *argv = NULL;
      if (buf[5] == 0)
	{
	  print_out("", "-!- CTCP Commands:");
	}
      else
	{
	  p = strchr(&buf[6], ' ');
	  if (p)
	    {
	      *p = 0;
	      recv = &buf[6];
	      command = p + 1;
	      p = strchr(command, ' ');
	      if (p)
		{
		  *p = 0;
		  argv = p + 1;
		}
	      command = upper(command);
	      snprintf(message, PIPE_BUF, "-!- CTCP request to %s: %s %s",
		       recv, command, argv ? argv : "");
	      print_out("", message);
	      if (argv)
		snprintf(message, PIPE_BUF, "PRIVMSG %s :\001%s %s\001\r\n",
			 recv, command, argv);
	      else
		snprintf(message, PIPE_BUF, "PRIVMSG %s :\001%s\001\r\n",
			 recv, command);
	    }
	  else
	    return;
	}
    }
  else if (!strncmp(&buf[1], "a", 1) || !strncmp(&buf[1], "away", 4))
    {
      char *begin = strchr(&buf[1], ' ');

      snprintf(message, PIPE_BUF, "-!- %s is away \"%s\"", nick,
	       begin ? begin : "");
      print_out(c->name, message);

      if (begin)
	{
	  begin++;
	  snprintf(message, PIPE_BUF, "AWAY :%s\r\n", begin);
	}
      else
	snprintf(message, PIPE_BUF, "AWAY\r\n");
    }
  else if (!strncmp(&buf[1], "dcc ", 4))
    {
      char *send[6] = { 0, 0, 0, 0, 0, 0 };

      tokenize(send, 6, &buf[1], ' ');
      if (!send[1])
	goto false_args;
      if (!strcmp(send[1], "list"))
	{
	  print_out("", "Num\tName\tIp\t\tPort\tFile\tSize");
	  for (l = dcc_send; l; l = l->next)
	    {
	      b = l->data;
	      snprintf(message, PIPE_BUF, "%d\t%s\t%s\t%s\t%s\t%s",
		       b->dcc_number, b->name, b->dcc_ip, b->dcc_port,
		       b->dcc_name, b->dcc_size);
	      print_out("", message);
	    }
	}
      else if (!strcmp(send[1], "close"))
	{
	  if (!send[2])
	    goto false_args;
	  int number = atoi(send[2]);
	  for (l = dcc_send; l; l = l->next)
	    {
	      b = l->data;
	      if (b->dcc_number == number)
		{
		  dcc_send = list_remove(dcc_send, b);
		  print_out("", "-!- Removed Filetransfer");
		  break;
		}
	    }
	}
      else if (!strcmp(send[1], "accept"))
	{
	  if (!send[2])
	    goto false_args;
	  int number = atoi(send[2]);
	  for (l = dcc_send; l; l = l->next)
	    {
	      b = l->data;
	      if (b->dcc_number == number)
		{
		  pid_t pid;
		  switch (pid = fork())
		    {
		    case -1:
		      print_out("", "-!- Error: Couldn't fork Filetransfer");
		      break;
		    case 0:
		      execlp(DCCPROG, "recv", "recv", b->dcc_ip, b->dcc_port,
			     b->dcc_name, path, b->name, NULL);
		      printf("Hail Diskordia! Hail Papa Loscher\n");
		      break;
		    default:
		      snprintf(message, PIPE_BUF,
			       "-!- Forked Filetransfer (Pid: %d)", pid);
		    }
		}
	    }
	}
      else if (!strcmp(send[1], "send"))
	{
	  if (!send[2] || !send[3])
	    goto false_args;
	  struct stat unused;
	  if (stat(send[3], &unused) == -1)
	    {
	      snprintf(message, PIPE_BUF, "-!- No Such file: %s", send[3]);
	      print_out("", message);
	      return;
	    }
	  pid_t pid;
	  switch (pid = fork())
	    {
	    case -1:
	      print_out("", "-!- Error: Couldn't fork Filetransfer");
	      break;
	    case 0:
	      execlp(DCCPROG, "send", "send", send[2], send[3],
		     send[4] ? send[4] : dcc_port, path, NULL);
	      printf("Hail Diskordia! Hail Papa Loscher\n");
	      break;
	    default:
	      snprintf(message, PIPE_BUF,
		       "-!- File transfer to %s was forked (Pid: %d)",
		       send[2], pid);
	      print_out("", message);
	    }

	  /* Sending CTCP DCC message */
	  char *ipnum;
	  ipnum = ip2num(ownip);
	  char *filename = strrchr(send[3], '/');
	  if (filename)
	    filename++;
	  else
	    filename = send[3];
	  snprintf(message, PIPE_BUF,
		   "PRIVMSG %s :\001DCC SEND %s %s %s %d\001\r\n", send[2],
		   filename, ipnum, send[4] ? send[4] : dcc_port,
		   (int) unused.st_size);
	  printf("%s\n", message);
	  write(irc, message, strlen(message));
	  free(ipnum);

	}
      return;
    false_args:
      print_out("", "-!- False arguments to dcc");
      print_out("", "-!- DCC commands:");
      print_out("", "-!- dcc send <nickname> <file> [port]");
      print_out("", "-!- dcc list");
      print_out("", "-!- dcc accept <number>");
      print_out("", "-!- dcc close <number>");

    }
  else if (!strncmp(&buf[1], "n ", 2))
    {
      snprintf(nick, sizeof(nick), "%s", &buf[3]);
      snprintf(message, PIPE_BUF, "NICK %s\r\n", &buf[3]);
    }
  else if (!strncmp(&buf[1], "l", 1))
    {
      if (c->name[0] == 0)
	return;
      if (buf[2] == ' ')
	snprintf(message, PIPE_BUF, "PART %s :%s\r\n", c->name, &buf[3]);
      else if (strlen(&buf[2]) == 0)
	snprintf(message, PIPE_BUF,
		 "PART %s :ii - thanks to papa loscher\r\n", c->name);
      else
	snprintf(message, PIPE_BUF, "%s\r\n", &buf[1]);
      write(irc, message, strlen(message));
      snprintf(message, PIPE_BUF, "-!- You left %s", c->name);
      print_out("", message);
      close(c->fd);
      create_filepath(infile, sizeof(infile), c->name, "in");
      unlink(infile);
      rm_channel(c);
      return;
    }
  else
    {
      snprintf(message, PIPE_BUF, "%s\r\n", &buf[1]);
    }
  write(irc, message, strlen(message));
}/*}}}*/
/*{{{ Server Input: CTCP*/
static void
proc_ctcp(char **argv)
{
  int i;
  for (i = 0; i < TOK_LAST; i++)
    printf("%s : ", argv[i]);
  putchar('\n');

  char *p = strchr(&argv[TOK_TEXT][1], '\001');
  if (p)
    *p = 0;
  char *sender = nil;
  if (argv[TOK_NICKSRV])
    sender = strdup(argv[TOK_NICKSRV]);
  char *command = strdup(&argv[TOK_TEXT][1]);
  NULL_TEST(command);
  if (!strncmp(command, "ACTION ", 7))
    {
      snprintf(message, PIPE_BUF, "* %s %s", argv[TOK_NICKSRV], &command[7]);
      if (!argv[TOK_CHAN] || !strncmp(argv[TOK_CHAN], nick, strlen(nick)))
	print_out("", message);
      else
	print_out(argv[TOK_CHAN], message);
    }
  else if (!strncmp(command, "FINGER", 6))
    {
      if (strlen(command) == 6)
	{			/* The other side wants a answer */
	  if (sender)
	    {
	      snprintf(message, PIPE_BUF, "-!- %s sent a FINGER request",
		       sender);
	      print_out("", message);
	      snprintf(message, PIPE_BUF,
		       "NOTICE %s :\001FINGER :%s\001\r\n", sender,
		       fullname ? fullname : nick);
	      write(irc, message, strlen(message));
	    }
	}
      else
	{
	  snprintf(message, PIPE_BUF, "-!- FINGER answer from %s: %s",
		   sender, &command[7]);
	  print_out("", message);

	}
    }
  else if (!strncmp(command, "VERSION", 7))
    {
      if (strlen(command) == 7)
	{
	  struct utsname kernel;
	  char sysname[PIPE_BUF - 50] = "unkown";
	  if (uname(&kernel) == 0)
	    {
	      snprintf(sysname, PIPE_BUF - 50, "%s %s [%s]", kernel.sysname,
		       kernel.release, kernel.machine);
	    }
	  snprintf(message, PIPE_BUF, "-!- %s sent a VERSION request",
		   sender);
	  print_out("", message);
	  snprintf(message, PIPE_BUF,
		   "NOTICE %s :\001VERSION ii:%s:%s\001\r\n", sender,
		   CLIENT_VERSION, sysname);
	  write(irc, message, strlen(message));
	}
      else
	{
	  snprintf(message, PIPE_BUF, "-!- VERSION answer from %s: %s",
		   sender, &command[8]);
	  print_out("", message);
	}
    }
  else if (!strncmp(command, "SOURCE", 6))
    {
      if (strlen(command) == 6)
	{
	  snprintf(message, PIPE_BUF, "-!- %s sent a SOURCE request", sender);
	  print_out("", message);
	  snprintf(message, PIPE_BUF, "NOTICE %s :\001SOURCE %s\001\r\n",
		   sender, CLIENT_SOURCE);
	  write(irc, message, strlen(message));
	}
      else
	{
	  snprintf(message, PIPE_BUF, "-!- SOURCE answer from %s: %s",
		   sender, &command[7]);
	  print_out("", message);
	}
    }
  else if (!strncmp(command, "USERINFO", 8))
    {
      if (strlen(command) == 8)
	{
	  snprintf(message, PIPE_BUF, "-!- %s sent a USERINFO request",
		   sender);
	  print_out("", message);
	  snprintf(message, PIPE_BUF, "NOTICE %s :\001USERINFO %s\001\r\n",
		   sender, USER_INFO);
	  write(irc, message, strlen(message));
	}
      else
	{
	  snprintf(message, PIPE_BUF, "-!- USERINFO answer from %s: %s",
		   sender, &command[9]);
	  print_out("", message);
	}
    }
  else if (!strncmp(command, "CLIENTINFO", 10))
    {
      if (strlen(command) == 10)
	{
	  snprintf(message, PIPE_BUF, "-!- %s sent a CLIENTINFO request",
		   sender);
	  print_out("", message);
	  snprintf(message, PIPE_BUF,
		   "NOTICE %s :\001CLIENTINFO ACTION FINGER VERSION SOURCE USERINFO CLIENTINFO ERRMSG TIME 'DCC SEND'\001\r\n",
		   sender);
	  write(irc, message, strlen(message));
	}
      else
	{
	  snprintf(message, PIPE_BUF, "-!- CLIENTINFO answer from %s: %s",
		   sender, &command[11]);
	  print_out("", message);
	}
    }
  else if (!strncmp(command, "ERRMSG", 6))
    {
      snprintf(message, PIPE_BUF, "-!- Error Message from %s: %s", sender,
	       &command[7]);
      print_out("", message);
    }
  else if (!strncmp(command, "DCC", 3))
    {
      char *send[6];
      if (tokenize(send, 6, command, ' ') == 6)
	{
	  if (!strcmp(send[1], "SEND"))
	    {
	      char *ip = num2ip(send[3]);
	      if (!ip)
		goto false_args;
	      snprintf(message, PIPE_BUF,
		       "-!- File offer from %s [%s:%s]: %s (%s bytes)",
		       sender, ip, send[4], send[2], send[5]);
	      print_out("", message);

	      Buddy *b = calloc(1, sizeof(Buddy));
	      NULL_TEST(b);
	      b->name = strdup(sender);
	      NULL_TEST(b->name);

	      b->dcc_port = strdup(send[4]);
	      NULL_TEST(b->dcc_port);

	      b->dcc_name = strdup(send[2]);
	      NULL_TEST(b->dcc_name);

	      b->dcc_size = strdup(send[5]);
	      NULL_TEST(b->dcc_size);
	      b->dcc_number = dcc_number++;
	      b->dcc_ip = ip;
	      dcc_send = list_append(dcc_send, b);
	    }
	  else
	    goto false_args;
	}
      else
	goto false_args;
      goto exit;
    false_args:
      snprintf(message, PIPE_BUF,
	       "NOTICE %s :\001ERRMSG DCC False arguments\001\n\r", sender);
      write(irc, message, strlen(message));
    }
  else if (!strncmp(command, "TIME", 4))
    {
      if (strlen(command) == 4)
	{
	  char timebuf[50];
	  snprintf(message, PIPE_BUF, "-!- %s sent a TIME request", sender);
	  print_out("", message);
	  time_t t = time(0);
	  strftime(timebuf, sizeof(timebuf), "%a %b %d %r %Y", localtime(&t));
	  snprintf(message, PIPE_BUF, "NOTICE %s :\001TIME %s\001\n\r",
		   sender, timebuf);
	  write(irc, message, strlen(message));
	}
      else
	{
	  snprintf(message, PIPE_BUF, "-!- TIME answer from %s: %s", sender,
		   &command[5]);
	  print_out("", message);
	}
    }
  else
    {
      snprintf(message, PIPE_BUF,
	       "NOTICE %s :\001ERRMSG Unkown Command: %s\001\n\r", sender,
	       command);
      write(irc, message, strlen(message));
    }
exit:
  free(sender);
  free(command);
}/*}}}*/
/*{{{ Server Input: Returning commands*/
static void
proc_cmd(char **argv)
{
  int cmd;
  char *p;
  Buddy *b;
  Channel *c;
  cmd = atoi(argv[TOK_CMD]);
  switch (cmd)
    {
    case 1: /* Get our own ip */
      p = strchr(argv[TOK_TEXT], '@');
      if (p)
	{
	  free(ownip);
	  struct hostent *host = gethostbyname(++p);
	  if (host)
	    ownip =
	      strdup(inet_ntoa(*((struct in_addr *) host->h_addr_list[0])));
	}
      break;
    case 301: /* Away */
      for (c = channels; c; c = c->next)
	if ((b = is_user_on(c, argv[TOK_ARG])))
	  {
	    if (!b->laststatus || strcmp(b->laststatus, argv[TOK_TEXT]))
	      {
		snprintf(message, PIPE_BUF, "* %s is %s", argv[TOK_ARG],
			 argv[TOK_TEXT]);
		free(b->laststatus);
		print_out("", message);
		b->laststatus = strdup(argv[TOK_TEXT]);
		NULL_TEST(b->laststatus);
	      }
	  }
      return;
    case 302: /* Get our own IP */
      p = strchr(argv[TOK_TEXT], '=');
      *p = 0;
      if (!strcmp(nick, argv[TOK_TEXT]))
	{
	  *p = '=';
	  p = strchr(++p, '@');
	  if (p)
	    {
	      free(ownip);
	      char *p1;
	      while ((p1 = strchr(p, ' ')) || (p1 = strchr(p, '\t')))
		*p1 = 0;
	      struct hostent *host = gethostbyname(++p);

	      if (host)
		ownip =
		  strdup(inet_ntoa
			 (*((struct in_addr *) host->h_addr_list[0])));
	    }
	}
      goto real_default;
      break;
    case 352: /* User joined */
      if ((c = get_channel(argv[TOK_ARG])))
	if (argv[TOK_NICK] && !is_user_on(c, argv[TOK_NICK]))
	  {
	    b = calloc(1, sizeof(Buddy));
	    NULL_TEST(b);
	    b->name = strdup(argv[TOK_NICK]);
	    NULL_TEST(b->name);
	    c->users = list_append(c->users, b);
	  }
      break;
    default:
      goto real_default;
    }

real_default:
  snprintf(message, PIPE_BUF, "%s", argv[TOK_TEXT] ? argv[TOK_TEXT] : "");
  print_out(0, message);
}/*}}}*/
/*{{{ Server Input */
static void
proc_server_cmd(char *buf)
{
  char *argv[TOK_LAST], *cmd, *p, *buffer;
  buffer = strdup(buf);
  Channel *c;
  List *l;
  Buddy *b;
  int i;
  if (!buf || *buf == '\0')
    return;

  for (i = 0; i < TOK_LAST; i++)
    argv[i] = nil;

  /*
     <message>  ::= [':' <prefix> <SPACE> ] <command> <params> <crlf>
     <prefix>   ::= <servername> | <nick> [ '!' <user> ] [ '@' <host> ]
     <command>  ::= <letter> { <letter> } | <number> <number> <number>
     <SPACE>    ::= ' ' { ' ' }
     <params>   ::= <SPACE> [ ':' <trailing> | <middle> <params> ]
     <middle>   ::= <Any *non-empty* sequence of octets not including SPACE
     or NUL or CR or LF, the first of which may not be ':'>
     <trailing> ::= <Any, possibly *empty*, sequence of octets not including NUL or CR or LF>
     <crlf>     ::= CR LF
   */
  if (buf[0] == ':')
    {				/* check prefix */
      p = strchr(buf, ' ');
      *p = 0;
      for (++p; *p == ' '; p++);
      cmd = p;
      argv[TOK_NICKSRV] = &buf[1];
      if ((p = strchr(buf, '!')))
	{
	  *p = 0;
	  argv[TOK_USER] = ++p;
	}
    }
  else
    cmd = buf;

  /* remove CRLFs */
  for (p = cmd; p && *p != 0; p++)
    if (*p == '\r' || *p == '\n')
      *p = 0;

  if ((p = strchr(cmd, ':')))
    {
      *p = 0;
      argv[TOK_TEXT] = ++p;
    }
  tokenize(&argv[TOK_CMD], TOK_LAST - TOK_CMD, cmd, ' ');




  if (!strncmp("PONG", argv[TOK_CMD], 5))
    {
      return;
    }
  else if (!strncmp("PING", argv[TOK_CMD], 5))
    {
      snprintf(message, PIPE_BUF, "PONG %s\r\n", argv[TOK_TEXT]);
      write(irc, message, strlen(message));
      return;
    }
  else if (!argv[TOK_NICKSRV] || !argv[TOK_USER])
    {				/* server command */
      proc_cmd(argv);
      return;
    }
  else if (!strncmp("ERROR", argv[TOK_CMD], 6))
    snprintf(message, PIPE_BUF, "-!- error %s",
	     argv[TOK_TEXT] ? argv[TOK_TEXT] : "unknown");
  else if (!strncmp("JOIN", argv[TOK_CMD], 5))
    {
      if (argv[TOK_TEXT] != nil)
	{
	  p = strchr(argv[TOK_TEXT], ' ');
	  if (p)
	    *p = 0;
	}
      argv[TOK_CHAN] = argv[TOK_TEXT];
      snprintf(message, PIPE_BUF, "-!- %s(%s) has joined %s",
	       argv[TOK_NICKSRV], argv[TOK_USER], argv[TOK_TEXT]);
      c = get_channel(argv[TOK_CHAN]);
      if (!c)
	{
	  add_channel(argv[TOK_CHAN]);
	  c = get_channel(argv[TOK_CHAN]);
	}
      if (argv[TOK_NICKSRV])
	{
	  b = calloc(1, sizeof(Buddy));
	  NULL_TEST(b);
	  b->name = strdup(argv[TOK_NICKSRV]);
	  NULL_TEST(b->name);
	  c->users = list_append(c->users, b);
	}
    }
  else if (!strncmp("PART", argv[TOK_CMD], 5))
    {
      snprintf(message, PIPE_BUF, "-!- %s(%s) has left %s",
	       argv[TOK_NICKSRV], argv[TOK_USER], argv[TOK_CHAN]);
      print_out(argv[TOK_CHAN], message);
      c = get_channel(argv[TOK_CHAN]);
      if (!c)
	return;
      if ((b = is_user_on(c, argv[TOK_NICKSRV])))
	{
	  c->users = list_remove(c->users, b);
	}
      return;
    }
  else if (!strncmp("MODE", argv[TOK_CMD], 5))
    snprintf(message, PIPE_BUF, "-!- %s changed mode/%s -> %s %s",
	     argv[TOK_NICKSRV], argv[TOK_CMD + 1], argv[TOK_CMD + 2],
	     argv[TOK_CMD + 3]);
  else if (!strncmp("QUIT", argv[TOK_CMD], 5))
    {
      snprintf(message, PIPE_BUF, "-!- %s(%s) has quit \"%s\"",
	       argv[TOK_NICKSRV], argv[TOK_USER],
	       argv[TOK_TEXT] ? argv[TOK_TEXT] : "");
      for (c = channels; c; c = c->next)
	if ((b = is_user_on(c, argv[TOK_NICKSRV])))
	  {
	    print_out(c->name, message);
	    c->users = list_remove(c->users, b);
	  }
      return;
    }
  else if (!strncmp("NICK", argv[TOK_CMD], 5))
    {
      snprintf(message, PIPE_BUF, "-!- %s changed nick to %s",
	       argv[TOK_NICKSRV],
	       argv[TOK_TEXT] ? argv[TOK_TEXT] : argv[TOK_CHAN]);
      for (c = channels; c; c = c->next)
	if ((b = is_user_on(c, argv[TOK_NICKSRV])))
	  {
	    print_out(c->name, message);
	    free(b->name);
	    b->name = strdup(argv[TOK_TEXT] ? argv[TOK_TEXT] :
			     (argv[TOK_CHAN] ? argv[TOK_CHAN] : ""));
	    NULL_TEST(b->name);
	  }
      return;
    }
  else if (!strncmp("TOPIC", argv[TOK_CMD], 6))
    snprintf(message, PIPE_BUF, "-!- %s changed topic to \"%s\"",
	     argv[TOK_NICKSRV], argv[TOK_TEXT] ? argv[TOK_TEXT] : "");
  else if (!strncmp("KICK", argv[TOK_CMD], 5))
    {
      snprintf(message, PIPE_BUF, "-!- %s kicked %s (\"%s\")",
	       argv[TOK_NICKSRV], argv[TOK_ARG],
	       argv[TOK_TEXT] ? argv[TOK_TEXT] : "");
      c = get_channel(argv[TOK_CHAN]);
      if (c)
	b = is_user_on(c, argv[TOK_ARG]);
      else
	return;

      if (l)
	c->users = list_remove(c->users, b);
    }
  else if (!strncmp("NOTICE", argv[TOK_CMD], 7))
    {
      if (argv[TOK_TEXT][0] == '\001')
	{
	  proc_ctcp(argv);
	  return;
	}
      else
	snprintf(message, PIPE_BUF, "-!- \"%s\")",
		 argv[TOK_TEXT] ? argv[TOK_TEXT] : "");
    }
  else if (!strncmp("PRIVMSG", argv[TOK_CMD], 8))
    {
      if (argv[TOK_TEXT] && argv[TOK_TEXT][0] == '\001')
	{			/* CTCP Messages */
	  proc_ctcp(argv);
	  return;
	}
      else
	snprintf(message, PIPE_BUF, "<%s> %s", argv[TOK_NICKSRV],
		 argv[TOK_TEXT] ? argv[TOK_TEXT] : "");
    }

  /* Possible Output */
  if (message[0] != 0)
    {
      if (!argv[TOK_CHAN] || !strncmp(argv[TOK_CHAN], nick, strlen(nick)))
	print_out("", message);
      else
	print_out(argv[TOK_CHAN], message);
    }

  /* Actual own IP */
  if (!ownip)
    {
      snprintf(message, PIPE_BUF, "USERHOST %s\r\n", nick);
      write(irc, message, strlen(message));
    }
}/*}}}*/
/*{{{ Readline form fd*/
static int
read_line(int fd, size_t res_len, char *buf)
{
  size_t i = 0;
  char c;
  do
    {
      if (read(fd, &c, sizeof(char)) != sizeof(char))
	return -1;
      buf[i++] = c;
    }
  while (c != '\n' && i < res_len);
  buf[i - 1] = 0;		/* eliminates '\n' */
  return 0;
}/*}}}*/
/*{{{ Meta input (server/user) */
static void
handle_channels_input(Channel * c)
{
  static char buf[PIPE_BUF];
  if (read_line(c->fd, PIPE_BUF, buf) == -1)
    {
      int fd = open_channel(c->name);
      if (fd != -1)
	c->fd = fd;
      else
	rm_channel(c);
      return;
    }
  proc_channels_input(c, buf);
}

static void
handle_server_output()
{
  static char buf[PIPE_BUF];
  if (read_line(irc, PIPE_BUF, buf) == -1)
    {
      perror("ii: remote host closed connection");
      exit(EXIT_FAILURE);
    }
  proc_server_cmd(buf);
}/*}}}*/
/*{{{ run */
static void
run()
{
  Channel *c;
  int r, maxfd;
  fd_set rd;
  struct timeval tv;
  char ping_msg[512];

  snprintf(ping_msg, sizeof(ping_msg), "PING %s\r\n", host);
  for (;;)
    {
      FD_ZERO(&rd);
      maxfd = irc;
      FD_SET(irc, &rd);
      for (c = channels; c; c = c->next)
	{
	  if (maxfd < c->fd)
	    maxfd = c->fd;
	  FD_SET(c->fd, &rd);
	}

      tv.tv_sec = 120;
      tv.tv_usec = 0;
      r = select(maxfd + 1, &rd, 0, 0, &tv);
      if (r < 0)
	{
	  if (errno == EINTR)
	    continue;
	  perror("ii: error on select()");
	  exit(EXIT_FAILURE);
	}
      else if (r == 0)
	{
	  if (time(NULL) - last_response >= PING_TIMEOUT)
	    {
	      print_out(NULL, "-!- ii shutting down: ping timeout");
	      exit(EXIT_FAILURE);
	    }
	  write(irc, ping_msg, strlen(ping_msg));
	  continue;
	}
      if (FD_ISSET(irc, &rd))
	{
	  handle_server_output();
	  last_response = time(NULL);
	}
      for (c = channels; c; c = c->next)
	if (FD_ISSET(c->fd, &rd))
	  handle_channels_input(c);
    }
}/*}}}*/
/*{{{ main*/
int
main(int argc, char *argv[])
{
  int i;
  unsigned short port = 6667;
  struct passwd *spw = getpwuid(getuid());
  char *key = nil;
  char prefix[_POSIX_PATH_MAX];


  if (!spw)
    {
      fprintf(stderr, "ii: getpwuid() failed\n");
      exit(EXIT_FAILURE);
    }
  snprintf(nick, sizeof(nick), "%s", spw->pw_name);
  snprintf(prefix, sizeof(prefix), "%s/irc", spw->pw_dir);

  if (argc == 2 && argv[1][0] == '-' && argv[1][1] == 'h')
    usage();

  for (i = 1; (i + 1 < argc) && (argv[i][0] == '-'); i++)
    {
      switch (argv[i][1])
	{
	case 'i':
	  snprintf(prefix, sizeof(prefix), "%s", argv[++i]);
	  break;
	case 's':
	  host = argv[++i];
	  break;
	case 'p':
	  port = atoi(argv[++i]);
	  break;
	case 'n':
	  snprintf(nick, sizeof(nick), "%s", argv[++i]);
	  break;
	case 'k':
	  key = argv[++i];
	  break;
	case 'f':
	  fullname = argv[++i];
	  break;
	case 'd':
	  dcc_port = argv[++i];
	  break;
	default:
	  usage();
	  break;
	}
    }
  irc = tcpopen(port);
  if (!snprintf(path, sizeof(path), "%s/%s", prefix, host))
    {
      fprintf(stderr, "%s", "ii: path to irc directory too long\n");
      exit(EXIT_FAILURE);
    }
  setenv("IRCDIR", prefix, 1);
  create_dirtree(path);
  add_channel("");		/* master channel */
  login(key, fullname);
  run();
  return 0;
}/*}}}*/
