#!/usr/bin/python

import os, sys, socket, struct
from time import localtime, strftime


if not os.environ.has_key("IRCDIR"):
    print "Fatal Error, this Program has to be started by ii"
    sys.exit(-1)
if len(sys.argv)<2:
    print "Fatal Error, too less arguments"
    sys.exit(-1)
if sys.argv[1]=="recv":
    if len(sys.argv)<7:
        print "Fatal Error, too less arguments"
        sys.exit(-1)
    host=sys.argv[2]
    port=int(sys.argv[3])
    name=sys.argv[4]
    outpath=sys.argv[5]
    nick=sys.argv[6]
    for res in socket.getaddrinfo(host, port, socket.AF_UNSPEC, socket.SOCK_STREAM):
        af, socktype, proto, canonname, sa = res
        try:
            s = socket.socket(af, socktype, proto)
        except socket.error, msg:
            s = None
            continue
        try:
            s.connect(sa)
        except socket.error, msg:
            s.close()
            print msg
            s = None
            continue
        break
    if s == None:
        print "Couldn't connect to host"
        sys.exit(-1)
    path=os.path.join(os.environ["IRCDIR"], "incoming")
    try:
        os.makedirs(path)
    except: pass
    path=os.path.join(path, name)
    while os.path.exists(path): path+="_"
    fd=open(path, "w+")
    recvbytes=0
    fd=open(os.path.join(outpath, "out"), "a+")
    fd.write("%s -!- File transfer started\n"%(strftime("%H:%M", localtime())))
    fd.close()
    while True:
        try:
            new_data = s.recv(2**14)
        except socket.error, x:
            # The server hung up.
            print "Recieved %d bytes"%recvbytes
            fd.close()
            sys.exit(0)
        fd.write(new_data)
        recvbytes += len(new_data)
        try:
            s.send(struct.pack("!I", recvbytes))
        except: break
    fd.close()
    fd=open(os.path.join(outpath, "out"), "a+")
    fd.write("%s -!- File transfer to %s was successful. File was written to %s\n"%(strftime("%H:%M", localtime()),nick, path))
    fd.close()
   
elif sys.argv[1]=="send": 
    if len(sys.argv)<6: 
        print "Fatal Error, too less arguments"
        sys.exit(-1)
    nick=sys.argv[2]
    name=sys.argv[3]
    port=int(sys.argv[4])
    outpath=sys.argv[5]
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.bind(("", port))
    print nick, name, port, outpath
    s.listen(1)
    conn, addr = s.accept()
    print "Accepted"
    try: 
        fd=open(name)
    except:
        conn.close()
        print "Fatal Error, no file"
        sys.exit(-1)
    while True:
        text=fd.read(1024)
        if len( text ) == 0:
            conn.close()
            fd=open(os.path.join(outpath, "out"), "a+")
            fd.write("%s -!- File transfer to %s was successful. \n"%(strftime("%H:%M", localtime()),nick))
            fd.close()
            sys.exit(-1)
        conn.send(text)
        conn.recv(2**7) # I will not use this crappy "error detection"

        
    
print "A real Error :("        
